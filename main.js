const electron = require('electron');
const url = require('url');
const path = require('path');
const {app, BrowserWindow, Menu} = require('electron');

let mainWindow;

// Listen for the app to be ready
app.on('ready', function(){
  // Create new Window
  mainWindow = new BrowserWindow({});
  // Load html into Window
  mainWindow.loadURL(`file://${__dirname}/html/splash.html`)

  // Build menu from Template
  const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
  // Insert Menu
  // Menu.setApplicationMenu(mainMenu);
});

function loadImporter(window){
  if (!window) {
    window = mainWindow;
  }
  window.loadURL(`file://${__dirname}/html/importer.html`)
}
exports.loadImporter = loadImporter;

const mainMenuTemplate = [
  {
    label: 'Migrator',
    submenu: [
        {
          label: 'import',
          click() {
            loadImporter(mainWindow);
          }
        },
        {
          label: 'Exit',
          click() {
            app.exit();
          }
        }
    ]
  }
];
